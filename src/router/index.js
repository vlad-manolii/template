import Vue from 'vue';
import Router from 'vue-router';
import adminRouters from './admin';
import publicRouters from './public';
import auth from "./auth"

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: () => import('@/layouts/default.vue'),
      children: [
        ...adminRouters,
        ...publicRouters,
      ],
    },
    ...auth,
    {
      path: '*',
      redirect: '/404',
    },
    {
      // the 404 route, when none of the above matches
      path: '/404',
      name: '404',
      meta: { title: 'ERROR' },
      component: () => import('@/pages/error/404.vue'),
    },
  ],
});

// router.beforeEach((to, from, next) => {
//   // Ensure we checked auth before each page load.
//   Promise.all([store.dispatch(VERIFY_AUTH)]).then(() => {
//     next()
//   })
// })

export default router
