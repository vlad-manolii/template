// Закрытые роуты
import user from "./user";

const routers = [
  {
    path: '/',
    redirect: '/user', // для этого примера редирект
    component: () => import('@/layouts/LayoutAdmin.vue'),
    children: [
      ...user,
    ],
  },
]
export default routers
