export default [
  {
    path: '/user',
    name: 'indexUser',
    meta: { title: 'indexUser' },
    component: () => import('@/pages/admin/user'),
  },
  {
    path: "/user/create",
    name: "createUser",
    meta: { title: 'createUser' },
    component: () => import('@/pages/admin/user/create'),
  },
  {
    path: "/user/:id",
    name: "viewUser",
    meta: { title: 'viewUser' },
    component: () => import('@/pages/admin/user/id'),
  },
  {
    path: "/user/:id/edit",
    name: "editUser",
    meta: { title: 'editUser' },
    component: () => import('@/pages/admin/user/id/edit'),
  },
]
