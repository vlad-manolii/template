const i18nService = {
  defaultLanguage: 'en',

  languages: [
    {
      lang: 'en',
      name: 'English',
    },
    {
      lang: 'ru',
      name: 'Russian',
    },
  ],

  /**
   * Keep the active language in the localStorage
   * @param lang
   */
  setActiveLanguage (lang) {
    localStorage.setItem('language', lang);
  },

  /**
   * Get the current active language
   * @returns {string | string}
   */
  getActiveLanguage () {
    return localStorage.getItem('language') || this.defaultLanguage;
  },
};

export default i18nService;
