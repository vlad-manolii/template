import axios from 'axios';
import { prepareParams, paramsSerializerQs } from '../helpers/api'
const ApiService = {
  constructor () {
    this.instance = null
  },
  init () {
    this.instance = axios.create({
      baseURL: `${process.env.VUE_APP_BACKEND}/api/v1`,
    });
    // Тут должна быть реализация interceptors для обработки рефреш токена и тд ...
  },
  request (url, params, cfg) {
    const config = { params: prepareParams(params), paramsSerializer: paramsSerializerQs, ...cfg }
    return this.instance.get(url, config);
  },
  get (url, params) {
    const config = { params: prepareParams(params), paramsSerializer: paramsSerializerQs }
    return this.instance.get(url, config)
  },
  postFile (url, data, params = {}) {
    const config = { ...params, headers: { 'Content-Type': 'multipart/form-data' } }
    return this.instance.post(url, data, config)
  },
  post (url, params) {
    return this.instance.post(`${url}`, params);
  },
  put (url, params) {
    return this.instance.put(`${url}`, params);
  },
  patch (url, params) {
    return this.instance.patch(`${url}`, params);
  },
  delete (url, data) {
    return this.instance.delete(url, { ...data })
  },
}
export default ApiService
