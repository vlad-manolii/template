import SuperRepository from '../superRepository';

export default new class UserRepository extends SuperRepository {
  baseUri () {
    return 'admin/user';
  }
}
