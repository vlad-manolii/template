// Частичная реализация авторизации

const state = {
  items: []
}

const getters = {
  getList: state => state.items,
  setItemById: (state) => (id) => {
    return state.items.find(todo => todo.id === id)
  }
}

const mutations = {
  setItem (state, data) {
    state.items = [].concat(state.items, data)
  },
  editItem (state, data) {
    state.items = state.items.map(item => {
      if (item.id === data.id) item = data
      return item
    })
  }
}

const actions = {
  onSave (context, payload) {
    const id = Math.floor(Math.random() * (1000 - 1 + 1) + 1) // random 1-1000
    const data = Object.assign({},{ id, ...payload })
    context.commit('setItem', data)
  },
  onEdit (context, payload) {
    context.commit('editItem', payload)
  },
  getUser (context, id) {

  }
}



export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
}
