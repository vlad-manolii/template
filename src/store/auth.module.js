// Частичная реализация авторизации

import ApiService from "../services/api.service"

const state = {
  user: {},
  isAuthenticated: true, // для примера всегда авторизирован
}

const getters = {
  isAuthenticated: state => state.isAuthenticated,
  getUser: state => state.user,
}

const actions = {
  onLogin (context, credentials) {
    return ApiService.post('oauth/token', postData)
  },
  onLogout (context) {
    context.commit('purgeAuth')
  },
  onRefreshToken () {
    const tokenData = 'получение токена'
    const oAuthRequired = {/* ключи */}
    const postData = { tokenData, ...oAuthRequired }
    return ApiService.post('oauth/token', postData)
  },
  onGetUser (context) {
    return new Promise(resolve => {
      ApiService.get('profile')
        .then(({ data }) => {
          context.commit('setUser', data.payload)
          resolve(data.payload)
        })
    })
  },
}

const mutations = {
  setUser (state, data) {
    state.user = data
  },
  setAuth (state, data) {
    state.isAuthenticated = true
    //...
  },
  purgeAuth (state) {
    state.isAuthenticated = false
    //...
  },
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
}
