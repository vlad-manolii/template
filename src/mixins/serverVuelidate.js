export default {
  data () {
    return {
      validations: {
        form: {}
      },
      serverErrors: {}
    }
  },
  validations() {
    return this.rules
  },
  computed: {
    rules() {
      return Object.assign({}, this.validations)
    }
  },
  methods: {
    validateState(name) {
      const { $dirty, $error } = this.$v.form[name];
      return $dirty ? !$error : null;
    },
    setServerErrors () {
      //
    },
    clearServerErrors () {
      //
    },
  }
}
